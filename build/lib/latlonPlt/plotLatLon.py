#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function

def plotFig(num1, num2, timeRange1, timeRange2, qAccuracy, sid):
    import cparser
    import logging
    from pathlib import Path
    from datetime import datetime as dt
    import gtimes.timefunc as tf
    import timesmatplt.timesmatplt as tplt  # plot GPS data?
    import tickLabels.functions as ft
    import GetComponents.plotComponents as plc
    import GetComponents.latlonPlot as plot
    import GetComponents.get_Data as data
    
    # the actual string input, keep that so we can look for it in the config file,
    # for example: there are GJOG and GJOG2 in the config but only GJOG in Database
    sidConfig = sid 
    
    # the real sid name that we want to look for in the database
    sid = sid.rsplit('2',1)[0] 
    
    dstr="%Y-%m-%d %H:%M:%S"
    dfstr="%Y-%m-%d"  # date format
    
    #Create the first time period
    start = tf.currDatetime(num1)
    start_date = dt.strftime(start, dstr)
    end_date =  plc.getEndDate(num2)
   
    #Create the second time period
    start2 = tf.currDatetime(timeRange1)
    start_date2 = dt.strftime(start2, dstr)
    end_date2 = plc.getEndDate(timeRange2)
    
    #Not using this at the moment
    StaPars = cparser.Parser()
    
    dt_start = tf.toDatetime(start_date,dstr) # datetime obj
    dt_end = tf.toDatetime(end_date,dstr)
    f_start = tf.currYearfDate(refday=dt_start) # float (fractional year)
    f_end = tf.currYearfDate(refday=dt_end)
    
    dt_start2 = tf.toDatetime(start_date2,dstr) # datetime obj
    dt_end2 = tf.toDatetime(end_date2,dstr)
    f_start2 = tf.currYearfDate(refday=dt_start2) # float (fractional year)
    f_end2 = tf.currYearfDate(refday=dt_end2)
    
    timestamp_format="%a %-d.%b %Y, %H:%M"
    timestamp=dt.now()
    
    constr = plc.getConstr(sidConfig)
    
    #Load the data from database
    
    #For GPS we need sid but not sidConfig because we need to look into the actual database but not the config folder...
    GPS = data.getGPS(sid, dt_end, dt_start) 
    Seis = data.getDataBase(dt_start, dt_end, sidConfig)
    Seis2 = data.getDataBase(dt_start2, dt_end2, sidConfig)
    
    #get the end of the plotting as pend, pend2 
    pend  = plc.getPend(num2, dt_end, dt_start)
    pend2 = plc.getPend(timeRange2, dt_end2, dt_start2)
    
    Title= plc.getColorWarnTitle()
    
    depth = True # to check for something in functions module...
    fig = ft.fourFrame(Ylabel=None, Title=Title, depth = depth)
    fig = ft.tstwofigTickLabelsFourFrame(fig,period=(pend-dt_start),period2=(pend2-dt_start2))
    
    supTitle="Jardskjalftamaelingar vid " + sid
    fig.suptitle(supTitle, fontsize=40, verticalalignment="center",x = 0.813,y = 0.935)
    
    #startTime = plc.setTimeperiodOnPlot(timeRange1, timeRange2)
    #supTitle2 = dates[0] + " - " + dates[1]
    
    ##### this line 
    #####
    #fig.suptitle(startTime, fontsize=40, verticalalignment="center",x = 0.2513,y = 0.935)
    
    Seis['Mcolors'] = plc.setSeisColors(Seis)
    Seis2['Mcolors'] = plc.setSeisColors(Seis2)
    
    Seis['Medge'] = float('nan')
    Seis.loc[ Seis["Q"] > qAccuracy, 'Medge' ] = 'black'
    Seis.loc[ Seis["Q"] <= qAccuracy, 'Medge' ] = 'face'
    
    #create two dataframes with Q values to distinguish how valid earthquake are...   
    Q2 = Seis2[ Seis2["Q"] > qAccuracy]
    Y2 = Seis2[ Seis2["Q"] <= qAccuracy]
    
    color_warn = plc.setColorWarn(dt_end)
    color_warn2 = plc.setColorWarn(dt_end2)
    
    # setting upp the axis stuff 
    seis_axis = fig.axes[0]
    seis_maxes = [seis_axis, seis_axis.twinx()]
    seis_axis.set_xlim([dt_start2, dt_end2])
    
    #Set a date title on the plot with gren/red color update 
    seis_maxes[0].set_title(Title, color=color_warn2, verticalalignment="bottom")
    
    seis_axisLat = fig.axes[1]
    seis_maxesLat = [seis_axisLat, seis_axisLat.twinx()]
    seis_axisLat.set_xlim([dt_start2, dt_end2])
    
    seis_axisDepth = fig.axes[2]
    seis_maxesDepth = [seis_axisDepth, seis_axisDepth.twinx()]
    seis_axisDepth.set_xlim([dt_start2, dt_end2])
    
    gps_axis = fig.axes[3]
    gps_maxes = [gps_axis, gps_axis.twinx(), gps_axis.twinx(),gps_axis.twinx()] 
    gps_axis.set_xlim([dt_start, dt_end])

    gps_maxes[2].spines['right'].set_position(("axes", 1.2))
    gps_maxes[2].set_frame_on(True)
    gps_maxes[2].patch.set_visible(True)
    
    fig.subplots_adjust(right=1.5)
    
    #Eartquake magnitude color label on the top graf
    plc.setpatch(seis_maxes)
    
    ###Pass 0 or 1 to check for lat or lon. 0 for longitude, 1 for latitude
    plc.setTimeperiodOnPlot(sidConfig, seis_axis, start2, dt_start2, 0)
    plc.setTimeperiodOnPlot(sidConfig, seis_axisLat, start2, dt_start2, 1)
    
    #landmarks and lines
    plc.setHorizLines(seis_maxes, seis_maxesLat, dt_end2, dt_start2, sidConfig, timeRange1)
    plc.plotLandmarks(seis_axis, seis_axisLat,timeRange1, sidConfig)
    
    plc.setVerticalUpdateLines(seis_maxes, seis_maxesLat, seis_maxesDepth , gps_maxes, dt_end, dt_end2 )
    
    # Plots Longitude, Latitude, Depth, Gps and Seismic moment
    # Q2 and Y2 store data for the second time period
    plot.plotLon(Q2, Y2, seis_maxes, constr,seis_axis , dt_start2, pend2,sidConfig)
    plot.plotLat(Q2, Y2, seis_maxesLat, dt_start2, pend2,sidConfig)
    plot.plotDepth(Q2, Y2, seis_maxesDepth, dt_start2, pend2)
    plot.plotGps(GPS,gps_maxes, gps_axis, start, pend, sid)
    plot.plotMomentAndcolorCodeQukes(Seis, start, pend, gps_maxes)
    
    #Set date and color coded update, green/red
    plc.annotateTimeUpdate(seis_maxesDepth, gps_maxes, dt_end, dt_end2, color_warn, color_warn2)
    
    # write to image file
    home = Path.home()
    
    relpath = "multiplot/figures/"
    filebase = sid + "1"
    filename = home.joinpath(relpath,filebase).as_posix()
    #filename = "/home/gpsops/multiplot/figures/multiplot"
    print(filebase + ".png created")
    tplt.saveFig(filename, "png", fig)
    #tplt.saveFig(filename, "pdf", fig)
    
    del fig
